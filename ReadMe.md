### Documentation
 "guard": "coded \> 0" : This is no longer true as guard is an object type


# Randoop Demo
This is a simple demonstration of how Randoop can be used to generate test

# Requirements
- Java 8

- Modified Randoop Maven plugin (original version [here](https://github.com/zaplatynski/randoop-maven-plugin), modified [here](https://bitbucket.org/adibar/randoop-maven/src/master/))
- Randoop v 4.0.4

# Run
- Run maven job : clean install
	- This will compile the main source code, generate Randoop tests and then run those tests
		 
# Points of interest
- class 'Teacher'
	- This class uses a representation invariant that all instances of 'Teacher' must satisfy else the instance has a bug.
- class 'Student'
	- This class is used to demonstrate the specifications feature of Randoop. The method 'enroll' is covered by a post condition that checks that when a student enrolls in the course, the course updates its student database.
