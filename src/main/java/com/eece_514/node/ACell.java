package com.eece_514.node;


public class ACell {
    public int data;
    public ACell next;

    /**
     * This constructor is added to  leverage Randoop's random input generation capability and to compensate for the lack
     * of Cute's input methods
     * @param data
     * @param next
     */
    public ACell(int data, ACell next) {
        this.data = data;
        this.next = next;
    }

    public int acceptOnlySmallerNodes(ACell aCell) {
        if (data > 0) {
            assert (aCell != null);
            if (exaggerate(data) < aCell.data) {
                assert (aCell.next != aCell);
            }

        }
        return 0;
    }

    int exaggerate(int value) {
        return 2 * value;
    }
}
