package com.eece_514.university;

import com.eece_514.university.model.Course;
import com.eece_514.university.model.Student;
import com.eece_514.university.model.Teacher;

public class Main {
    public static void main(String[] args) {
        Teacher teacher_johnDoe = new Teacher("John Doe");
        Teacher teacher_applebaum = new Teacher("AppleBaum");

        Course course_fourthDimension = new Course("Intro to the 4th dimension", teacher_applebaum);
        Course course_fourthDimension_advanced = new Course("4th dimension - Advanced Studies", teacher_johnDoe);

        Student student_tonyStark = new Student("Tony Stark");
        Student student_drStrange = new Student("Dr Strange");

        student_drStrange.enroll(course_fourthDimension);
        student_drStrange.enroll(course_fourthDimension_advanced);

        student_tonyStark.enroll(course_fourthDimension);

        teacherInteraction(student_drStrange);
        teacherInteraction(student_tonyStark);



    }

    private static void teacherInteraction(Student student) {
        System.out.println(String.format("%s interacts with %d teachers", student.getName(), student.teachers().size()));
    }
}
