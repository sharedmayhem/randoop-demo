package com.eece_514.university.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class Course {
    private String name;
    private Teacher teacher;
    private Collection<Student> students = new ArrayList<>();

    public Course(String name, Teacher teacher) {
        this.name = name;
        this.teacher = teacher;
        this.teacher.teaches(this);
    }

    public String getName() {
        return name;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(name, course.name) &&
                Objects.equals(teacher, course.teacher);
    }

    boolean enrolledBy(Student student) {
        return students.add(student);
    }

    public Collection<Student> getStudents(){
        return students;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, teacher);
    }
}
