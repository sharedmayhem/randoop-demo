package com.eece_514.university.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Collectors;

public class Student {
    private String name;
    private Collection<Course> courses = new HashSet<>();

    public Student(String name) {
        this.name = name;
    }

    public Student enroll(Course course){
        if(course == null){
            throw new RuntimeException("You must specify a course");
        }
        courses.add(course);
        course.enrolledBy(this);
        return this;
    }

    public String getName() {
        return name;
    }

    public Collection<Teacher> teachers(){
        return courses.stream().map(Course::getTeacher).collect(Collectors.toList());
    }

    public boolean isEnrolledInCourse(Course course){
        return courses.contains(course);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
