package com.eece_514.university.model;

import randoop.CheckRep;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class Teacher {
    private static int count = 0;
    private Random random = new Random(0);
    private String name;
    private Collection<Course> courses = new ArrayList<>();

    public Teacher(String name) {
        this.name = name;
        System.out.println("Generated instance count of Teacher " + ++count);
    }

    public String getName() {
        return name;
    }

    @CheckRep
    public boolean verifyCourseIsTaughtByTeacher() {
        System.out.println("Calling invariant");
        Boolean result;
        result = courses.stream().map(crs -> crs.getTeacher().equals(this)).reduce(true, (val1, val2) -> val1 && val2);
        return result;
    }

    public Collection<Course> getCoursesTaught() {
        return Collections.unmodifiableCollection(courses);
    }

    //This method, if public, causes Randoop to generate failing test
    void teaches(Course course) {
        courses.add(course);
    }
}
